<?php
	require_once "config.php";
	
	/*if(!isset($_SESSION["user_email"]))
	{
		header("location: index.php");
		exit;
	}*/
?>   
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Video AWS</title>
<style>
html, body{
    height:100%;
}
body{
    margin:0;
    padding:0;
}
#player{
    width:100%;
    height:100vh;
}
</style>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/clappr@latest/dist/clappr.min.js"></script>

</head>

<body>
<div id="player"></div>
  <script type="text/javascript" src="//cdn.jsdelivr.net/gh/clappr/clappr-level-selector-plugin@latest/dist/level-selector.min.js"></script>
  <script>
    var player = new Clappr.Player(
    {
        source: "https://d28fp6yvlbvtrp.cloudfront.net/out/v1/4efca99fc2194cd1ae25b0bde7b4ff31/b717f2d247e14fb795a7f1556df54c1b/441c57f563144cab9a67f07c73433a7f/index.m3u8",      
        parentId: "#player",
        plugins: [LevelSelector],
        levelSelectorConfig: {
          title: 'Quality',
          labels: {
              1: '360p', // 500kbps
              0: '180p', // 240kbps
          },
          labelCallback: function(playbackLevel, customLabel) {
              return customLabel;// + playbackLevel.level.height+'p'; // High 720p
          }
        },
        //poster: "img/poster.jpg",
        width: "100%",
        height: "100%",
        
    });
    
    player.play();
</script>
</body>
</html>